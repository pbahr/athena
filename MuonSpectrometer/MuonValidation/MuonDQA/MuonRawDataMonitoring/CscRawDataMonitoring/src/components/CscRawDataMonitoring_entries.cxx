#include "CscRawDataMonitoring/CscRdoValAlg.h"
#include "CscRawDataMonitoring/CscPrdValAlg.h"
#include "CscRawDataMonitoring/CscClusterValAlg.h"
#include "CscRawDataMonitoring/CSCSegmValAlg.h"
  
DECLARE_COMPONENT( CscRdoValAlg )
DECLARE_COMPONENT( CscPrdValAlg )
DECLARE_COMPONENT( CscClusterValAlg )
DECLARE_COMPONENT( CSCSegmValAlg )

